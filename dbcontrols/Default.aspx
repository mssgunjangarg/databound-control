﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>

        <asp:DataList ID="DataList1" runat="server" Width="100%">
            <HeaderTemplate>
                <table border="0" style="width:100%">
                    <tr>
                        <th style="width:20%">Class</th>
                        <th style="width:20%">General
                            
                            <table border="0"  style="width:100%">
                                <tr>
                                    <th>Male</th>
                                    <th>Female</th>

                                </tr>


                            </table>
                        </th>
                        <th style="width:20%">SC
                            <br />
                                <table border="0" style="width:100%">
                                <tr>
                                    <th>Male</th>
                                    <th>Female</th>

                                </tr>


                            </table>
                        </th>
                        <th style="width:20%">ST
                            <br />
                                <table border="0" style="width:100%">
                                <tr>
                                    <th>Male</th>
                                    <th>Female</th>

                                </tr>


                            </table>
                        </th>
                        <th style="width:20%">OBC
                                <table border="0" style="width:100%">
                                <tr>
                                    <th>Male</th>
                                    <th>Female</th>

                                </tr>


                            </table>
                        </th>
                    </tr>
                    
              
            </HeaderTemplate>
            <ItemTemplate>
                  <tr>
                        <td><%# Eval("Class") %></td>
                        <td >
                           <asp:Label Text='<%# Convert.ToString(Eval("General")) %>' 
         style="width:50%"      runat="server" />
                        </td>
                        <td ><asp:Label Text='<%# Eval("SC").ToString() == "" ? "0" : Eval("SC") %>'
                    runat="server" /></td>
                      <td ><asp:Label Text='<%# Eval("ST").ToString() == "" ? "0" : Eval("ST") %>'
                    runat="server" /></td>
                      <td><asp:Label Text='<%# Eval("OBC").ToString() == "" ? "0" : Eval("OBC") %>'
                    runat="server" /></td>
                    </tr>



            </ItemTemplate>
            <FooterTemplate>
                  </table>

            </FooterTemplate>
        </asp:DataList>


    </div>
    </form>
</body>
</html>
