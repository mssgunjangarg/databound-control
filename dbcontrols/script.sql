USE [User]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[UserDetail](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Class] [varchar](50) NULL,
	[Category] [varchar](50) NULL,
	[MaleCount] [int] NULL,
	[FemaleCount] [int] NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[UserDetail] ON 

INSERT [dbo].[UserDetail] ([Id], [Class], [Category], [MaleCount], [FemaleCount]) VALUES (1, N'B.Tech', N'General', 20, 15)
INSERT [dbo].[UserDetail] ([Id], [Class], [Category], [MaleCount], [FemaleCount]) VALUES (2, N'BBA', N'SC', 30, 32)
INSERT [dbo].[UserDetail] ([Id], [Class], [Category], [MaleCount], [FemaleCount]) VALUES (3, N'MBA', N'ST', 28, 18)
INSERT [dbo].[UserDetail] ([Id], [Class], [Category], [MaleCount], [FemaleCount]) VALUES (4, N'BA', N'OBC', 35, 10)
INSERT [dbo].[UserDetail] ([Id], [Class], [Category], [MaleCount], [FemaleCount]) VALUES (5, N'BA', N'SC', 34, 88)
INSERT [dbo].[UserDetail] ([Id], [Class], [Category], [MaleCount], [FemaleCount]) VALUES (6, N'BA', N'General', 85, 78)
INSERT [dbo].[UserDetail] ([Id], [Class], [Category], [MaleCount], [FemaleCount]) VALUES (7, N'MBA', N'SC', 40, 80)
INSERT [dbo].[UserDetail] ([Id], [Class], [Category], [MaleCount], [FemaleCount]) VALUES (8, N'MBA', N'OBC', 57, 87)
INSERT [dbo].[UserDetail] ([Id], [Class], [Category], [MaleCount], [FemaleCount]) VALUES (9, N'B.Tech', N'SC', 75, 99)
INSERT [dbo].[UserDetail] ([Id], [Class], [Category], [MaleCount], [FemaleCount]) VALUES (10, N'B.Tech', N'OBC', 35, 28)
INSERT [dbo].[UserDetail] ([Id], [Class], [Category], [MaleCount], [FemaleCount]) VALUES (11, N'BBA', N'ST', 67, 88)
INSERT [dbo].[UserDetail] ([Id], [Class], [Category], [MaleCount], [FemaleCount]) VALUES (12, N'BBA', N'General', 44, 22)
SET IDENTITY_INSERT [dbo].[UserDetail] OFF
